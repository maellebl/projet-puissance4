################################################################################
# Votre travail consiste à compléter les fonctions suivantes afin de pouvoir   #
# afficher la grille dans la console et de rétablir la gravité pour les pions  #
################################################################################
# Il s'agit bien sûr de respecter scrupuleusement les specifications.
# Un commentaire pour les lignes de code non triviales est exigé.
# Pour tester votre script, il suffit d'exécuter le programme JeuPuissance4.
# Le jeu se joue avec la souris uniquement, les joueurs se la passe à tour de role.
# Si vos fonctions sont justes alors le jeu fonctionnera !
# Bon courage.


def affichage_console(grille):
    ''' Cette fonction affiche la grille dans la console.
        Chaque ligne de la grille est affichée sur une ligne différente dans la console.
        Un séparateur est affiché après la grille.
    '''
    for i in grille : 
        print (i) 
    print ('#####################')    
    
    

def case_libre_la_plus_basse(grille,num_colonne):
    ''' Cette fonction renvoie la case libre la plus basse de la colonne donnée.
        Si la colonne est pleine, cette fonction renverra None.
    entrée:
        grille: tableau de 6 lignes et 7 colonnes représentant le plateau du jeu
        num_colonne: numéro de la colonne à analyser
    return: (num_ligne,num_colonne) correspondant au numéro de ligne et au
        numéro de colonne de la case vide la plus basse dans la colonne donnée.
    '''
#    if grille[0][num_colonne]!=0:
#        print('replie')
#        return None 
    for j in range (0,6):
        if grille[j][num_colonne]!=0 : 
            print(j-1)
            if j==0 : return None
            return ( j-1 , num_colonne )
        if j==5 : 
            return 5, num_colonne

               
    
    
    


















